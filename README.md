# pruefung_m169_5ip22b

1. Als erstes forken Sie dieses Projekt. 	Erledigt

1. Erstellen Sie sich eine lokale Kopie vom neuen Projekt 	Erledigt

1. Erstellen Sie eine lokale Datei `name.txt`, es soll Ihr `Vorname Nachname` enthalten. 	Erledigt

1. Erstellen Sie eine lokale Datei `addresses.txt` mit dem Inhalt des Commands `ip a`. 	Erledigt
 
1. Fügen Sie diese Dateien in Ihr Repository und setzen Sie eine geeignete Meldung dazu. 	Erledigt

1. Schreiben Sie das Ergebnis wieder ins Gitlab. 1.	Erledigt

1. Das Repository soll später ein Java-Projekt enthalten. Definieren Sie, dass `.class`, `.log` und `.jar` Files nicht eingepflegt werden. 	Erledigt

1. Erstellen Sie einen Branch, nennen Sie ihn `1.1`. 	Erledigt